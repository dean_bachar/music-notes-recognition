from typing import List

import cv2
import numpy as np
import tensorflow as tf
from numpy import ndarray

# Files
VOCABULARY_FILE_PATH = 'content/vocabulary_agnostic.txt'
SAVED_MODEL_PATH = 'content/agnostic_model.meta'

# Special globals
sess = tf.InteractiveSession()
saver = tf.train.import_meta_graph(SAVED_MODEL_PATH)
saver.restore(sess, SAVED_MODEL_PATH[:-5])
graph = tf.get_default_graph()

# Get tensors
input_tensor = graph.get_tensor_by_name("model_input:0")
seq_len_tensor = graph.get_tensor_by_name("seq_lengths:0")
rnn_keep_prob_tensor = graph.get_tensor_by_name("keep_prob:0")
height_tensor = graph.get_tensor_by_name("input_height:0")
width_reduction_tensor = graph.get_tensor_by_name("width_reduction:0")
logits = tf.get_collection("logits")[0]
WIDTH_REDUCTION, HEIGHT = sess.run([width_reduction_tensor, height_tensor])
decoded, _ = tf.nn.ctc_greedy_decoder(logits, seq_len_tensor)

# Generate note by index translator
with open(VOCABULARY_FILE_PATH, 'r') as file:
    lines = file.read().splitlines()
    notes_by_indexes = {count: line for count, line in enumerate(lines)}


def convert_image_string_to_np_array(img_str: str) -> ndarray:
    np_array = np.fromstring(img_str, np.uint8)
    img_np = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
    img_np = _increase_brightness(img_np)
    img_np = cv2.cvtColor(img_np, cv2.COLOR_BGR2GRAY)
    return img_np


def predict(img: ndarray) -> List[str]:
    new_image = _resize(img, HEIGHT)
    new_image = _normalize(new_image)
    new_image = np.asarray(new_image).reshape(1, new_image.shape[0], new_image.shape[1], 1)

    seq_lengths = [new_image.shape[2] / WIDTH_REDUCTION]
    prediction = sess.run(decoded,
                          feed_dict={
                              input_tensor: new_image,
                              seq_len_tensor: seq_lengths,
                              rnn_keep_prob_tensor: 1.0,
                          })
    str_predictions = _sparse_tensor_to_strs(prediction)
    return [notes_by_indexes[prediction_number] for prediction_number in str_predictions[0]]


def _increase_brightness(img, value=100):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img


def _normalize(image):
    return (255. - image) / 255.


def _resize(image, height):
    width = int(float(height * image.shape[1]) / image.shape[0])
    sample_img = cv2.resize(image, (width, height))
    return sample_img


def _sparse_tensor_to_strs(sparse_tensor):
    indices = sparse_tensor[0][0]
    values = sparse_tensor[0][1]
    dense_shape = sparse_tensor[0][2]

    strs = [[] for _ in range(dense_shape[0])]

    string = []
    ptr = 0
    b = 0

    for idx in range(len(indices)):
        if indices[idx][0] != b:
            strs[b] = string
            string = []
            b = indices[idx][0]

        string.append(values[ptr])

        ptr = ptr + 1

    strs[b] = string

    return strs
