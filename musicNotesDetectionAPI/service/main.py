from typing import List
from typing import Optional

from expiringdict import ExpiringDict
from fastapi import FastAPI, File, UploadFile, Request
from starlette.datastructures import Address

from prediction import predict, convert_image_string_to_np_array

app = FastAPI()
cache: Optional[ExpiringDict] = None


def get_key(pre: List[str], client: Address) -> str:
    return f"{pre} {client.host}"


@app.post("/predict")
def create_file(request: Request, image: UploadFile = File(...)) -> List[str]:
    np_img = convert_image_string_to_np_array(image.file.read())
    pre = predict(np_img)
    key = get_key(pre, request.client)
    last_value = cache.get(key)
    cache[key] = True
    value = [] if last_value else pre
    return value


@app.on_event("startup")
def startup_event():
    global cache
    cache = ExpiringDict(max_len=3000, max_age_seconds=6)
